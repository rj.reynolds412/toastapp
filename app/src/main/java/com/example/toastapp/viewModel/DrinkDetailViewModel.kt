package com.example.toastapp.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.toastapp.domain.GrabDrinkDetailUseCase
import com.example.toastapp.local.dao.entity.Drinks
import com.example.toastapp.state.DrinkDetailState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class DrinkDetailViewModel @Inject constructor(
    val grabDrinkDetailUseCase: GrabDrinkDetailUseCase,
) : ViewModel() {


    private val _detailState = MutableLiveData(DrinkDetailState(isLoading = true))
    val detailState: LiveData<DrinkDetailState> get() = _detailState

    fun grabDrinkDetails(drinkId: Int) {
        viewModelScope.launch {
            val detailResult = grabDrinkDetailUseCase(drinkId)
            if (detailResult.isSuccess) {
                val drinkDetails = detailResult.getOrDefault(Drinks())
                _detailState.value = DrinkDetailState(
                    drinkDetails = drinkDetails,
                    isLoading = false,
                )
            } else {
                _detailState.value = DrinkDetailState(
                    errorMsg = detailResult.exceptionOrNull()?.message,
                    isLoading = false
                )
            }
        }
    }
}