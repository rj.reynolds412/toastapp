package com.example.toastapp.viewModel

import androidx.lifecycle.*
import com.example.toastapp.domain.GrabCategoryUseCase
import com.example.toastapp.state.CategoryState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class DrinkCategoryViewModel @Inject constructor(
    val grabCategoryUseCase: GrabCategoryUseCase,
) : ViewModel() {
    private val _categoryState = MutableLiveData(CategoryState(isLoading = true))
    val categoryState: LiveData<CategoryState> get() = _categoryState

    fun getCategory(category: String){
        viewModelScope.launch {
            val categoryResult = grabCategoryUseCase(category)
            if (categoryResult.isSuccess){
                val categories = categoryResult.getOrDefault(listOf())
                _categoryState.value = CategoryState(
                    categoryResponse = categories,
                    isLoading = false
                )
            } else{
                _categoryState.value = CategoryState(
                    errorMsg = categoryResult.exceptionOrNull()?.message,
                    isLoading = false
                )
            }
        }
    }
}
