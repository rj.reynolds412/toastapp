package com.example.toastapp.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.toastapp.domain.GrabCategoryDrinksUseCase
import com.example.toastapp.domain.SavedFavoritesUseCase
import com.example.toastapp.local.dao.entity.Drinks
import com.example.toastapp.model.ToastRepo
import com.example.toastapp.state.CategoryDrinkState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class CategoryDrinksViewModel @Inject constructor(
    val grabCategoryDrinksUseCase: GrabCategoryDrinksUseCase,
    val savedFavoritesUseCase: SavedFavoritesUseCase) : ViewModel() {

    private val _drinkState = MutableLiveData(CategoryDrinkState(isLoading = true))
    val drinkState: LiveData<CategoryDrinkState> get() = _drinkState

    fun getCategoryDrinks(categoryDrinks: String) {
        viewModelScope.launch {
            val result = grabCategoryDrinksUseCase(categoryDrinks)
            if (result.isSuccess) {
                val listDrinks = result.getOrDefault(listOf())
                _drinkState.value = CategoryDrinkState(
                    drinksList = listDrinks,
                    isLoading = false
                )
            } else {
                _drinkState.value = CategoryDrinkState(
                    errorMsg = result.exceptionOrNull()?.message,
                    isLoading = false
                )
            }
        }
    }

    fun getFavoriteDrinks(favorites: Boolean){
        viewModelScope.launch {
            val favorite = savedFavoritesUseCase(favorites)
            if (favorite.isSuccess){
                val listFavs = favorite.getOrDefault(Drinks())
               listFavs.isFavorited = true
            }else{
                favorite.isFailure
            }
        }
    }

}