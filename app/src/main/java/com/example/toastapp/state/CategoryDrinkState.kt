package com.example.toastapp.state

import com.example.toastapp.local.dao.entity.Drinks

data class CategoryDrinkState(
    val isLoading: Boolean = false,
    val drinksList: List<Drinks> = emptyList(),
    val errorMsg: String? = null,
)
