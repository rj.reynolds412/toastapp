package com.example.toastapp.state

import com.example.toastapp.local.dao.entity.Drinks

data class DrinkDetailState(
    val isLoading: Boolean = false,
    val drinkDetails: Drinks = Drinks(),
    val errorMsg: String? = null,
)