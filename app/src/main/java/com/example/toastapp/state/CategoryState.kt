package com.example.toastapp.state

import com.example.toastapp.local.dao.entity.Category

data class CategoryState(
    val isLoading: Boolean = false,
    val categoryResponse: List<Category> = emptyList(),
    val errorMsg: String? = null
)
