package com.example.toastapp.domain

import com.example.toastapp.local.dao.entity.Drinks
import com.example.toastapp.model.ToastRepo
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class GrabDrinkDetailUseCase @Inject constructor(
    private val toastRepo: ToastRepo,
) {

    suspend operator fun invoke(drinkId: Int): Result<Drinks> =
        withContext(Dispatchers.IO) {
            return@withContext try {
                val detailResponse: Drinks = toastRepo.getDrinkById(drinkId)
                Result.success(detailResponse)
            } catch (ex: Exception) {
                Result.failure(ex)
            }
        }
}