package com.example.toastapp.domain

import com.example.toastapp.local.dao.entity.Drinks
import com.example.toastapp.model.ToastRepo
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class GrabCategoryDrinksUseCase @Inject constructor(
    private val toastRepo: ToastRepo) {

    suspend operator fun invoke(drinkCategory : String): Result<List<Drinks>> =
        withContext(Dispatchers.IO){
            return@withContext try {
                val drinkResponse: List<Drinks> =
                    toastRepo.getCategoryDrinks(drinkCategory)
                Result.success(drinkResponse)
            }catch (ex: Exception){
                Result.failure(ex)
            }
        }
}