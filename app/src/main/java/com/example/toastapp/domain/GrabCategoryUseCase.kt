package com.example.toastapp.domain

import com.example.toastapp.local.dao.entity.Category
import com.example.toastapp.model.ToastRepo
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class GrabCategoryUseCase @Inject constructor(
    private val toastRepo: ToastRepo
    ) {

    suspend operator fun invoke(category: String): Result<List<Category>> =
        withContext(Dispatchers.IO) {
            return@withContext try {
                val categoryResponse: List<Category> =
                    toastRepo.getCategory(category)
                Timber.e("the result of get category was $categoryResponse")
                Result.success(categoryResponse)
            } catch (ex: Exception) {
                Result.failure(ex)
            }
        }
}