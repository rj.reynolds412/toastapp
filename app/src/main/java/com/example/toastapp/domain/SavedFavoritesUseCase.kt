package com.example.toastapp.domain

import com.example.toastapp.local.dao.entity.Drinks
import com.example.toastapp.model.ToastRepo
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SavedFavoritesUseCase @Inject constructor(
    private val toastRepo: ToastRepo
) {
    suspend operator fun invoke(favorites : Boolean) : Result<Drinks> =
        withContext(Dispatchers.IO){
            return@withContext try {
                val selectedFav : Drinks = toastRepo.getFavoriteDrinks(favorites)
                Result.success(selectedFav)
            }catch (ex: Exception){
                Result.failure(ex)
            }
        }
}