package com.example.toastapp.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.example.toastapp.databinding.ItemDrinkBinding
import com.example.toastapp.local.dao.entity.Drinks

class CategoryDrinksAdapter(
    val drinkClicked: (drink: Drinks) -> Unit,
    val favClicked: (drink: Drinks) -> Unit,
) :
    RecyclerView.Adapter<CategoryDrinksAdapter.CategoryDrinksViewHolder>() {
    private val drinks = mutableListOf<Drinks>()

    class CategoryDrinksViewHolder(
        val binding: ItemDrinkBinding,
    ) : RecyclerView.ViewHolder(binding.root) {
        fun grabCategoryDrinks(drinks: Drinks) {
            binding.ivDrink.load(drinks.strDrinkThumb)
            binding.tvDrink.text = drinks.strDrink
        }

        companion object {
            fun getInstance(parent: ViewGroup) = ItemDrinkBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            ).let { CategoryDrinksViewHolder(it) }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryDrinksViewHolder {
        return CategoryDrinksViewHolder.getInstance(parent).apply {
            itemView.setOnClickListener {
                drinkClicked(drinks[adapterPosition])
            }
        }
    }

    override fun onBindViewHolder(holder: CategoryDrinksViewHolder, position: Int) {
        val drinks = drinks[position]
        holder.grabCategoryDrinks(drinks)
        holder.binding.ivFav.setOnClickListener{
            favClicked(drinks)
        }
    }

    override fun getItemCount(): Int = drinks.size

    fun displayDrinks(drinks: List<Drinks>) {
        this.drinks.run {
            val oldSize = size
            clear()
            notifyItemRangeRemoved(0, oldSize)
            addAll(drinks)
            notifyItemRangeInserted(0, drinks.size)
        }
    }
}