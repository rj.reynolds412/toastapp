package com.example.toastapp.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.toastapp.databinding.ItemCategoryBinding
import com.example.toastapp.local.dao.entity.Category
import com.example.toastapp.view.DrinkCategoryFragmentDirections
import timber.log.Timber

class CategoryAdapter :
    RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder>() {

    private var category = mutableListOf<Category>()

    class CategoryViewHolder(
        val binding: ItemCategoryBinding,
    ) : RecyclerView.ViewHolder(binding.root) {

        fun grabCategory(categories: Category) {
            Timber.d("grabCategory: " + categories.strCategory + " ")
            binding.tvCategory.text = categories.strCategory
        }

        companion object {
            fun getInstance(parent: ViewGroup) = ItemCategoryBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            ).let { CategoryViewHolder(it) }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryViewHolder {
        return CategoryViewHolder.getInstance(parent)
    }

    override fun onBindViewHolder(holder: CategoryViewHolder, position: Int) {
        val category = category[position]
        holder.grabCategory(category)
        holder.binding.tvCategory.setOnClickListener { nav ->
            nav.findNavController().navigate(
                DrinkCategoryFragmentDirections.actionDrinkCategoryFragmentToDrinkListFragment(
                    category.strCategory))
        }
    }

    override fun getItemCount(): Int = category.size

    fun displayCategory(categories: List<Category>) {
        this.category.run {
            val prevSize = size
            clear()
            notifyItemRangeRemoved(0, prevSize)
            addAll(categories)
            Timber.d("displayCategory: " + categories.size + " ")
            notifyItemRangeInserted(0, categories.size)
        }
    }
}