package com.example.toastapp.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import coil.load
import com.example.toastapp.databinding.FragmentDrinkDetailBinding
import com.example.toastapp.domain.GrabDrinkDetailUseCase
import com.example.toastapp.factory.DrinkDetailViewModelFactory
import com.example.toastapp.local.dao.entity.Drinks
import com.example.toastapp.viewModel.DrinkDetailViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class DrinkDetailFragment : Fragment() {

    private var _binding: FragmentDrinkDetailBinding? = null
    private val binding get() = _binding!!
    private val args by navArgs<DrinkDetailFragmentArgs>()

    @Inject
    lateinit var useCase: GrabDrinkDetailUseCase
    private val drinkDetailViewModel by viewModels<DrinkDetailViewModel> {
        DrinkDetailViewModelFactory(useCase)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ) = FragmentDrinkDetailBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initObservers()
    }


    private fun initObservers() {
        val drinkId = args.drinkId
        drinkDetailViewModel.detailState.observe(viewLifecycleOwner) { state ->
            val drink: Drinks = state.drinkDetails
            displayDeets(drink)
        }
        drinkDetailViewModel.grabDrinkDetails(drinkId)
    }

    private fun displayDeets(drink: Drinks) = with(binding) {
        drink.run {
            ivDetail.load(strDrinkThumb)
            tvDetailTitle.text = drink.strDrink
            val details = listOfNotNull(
                strIngredient1,
                strIngredient2,
                strIngredient3,
                strIngredient4,
                strMeasure1,
                strMeasure2,
                strMeasure3,
                strInstructions)
            tvDetail.text = details.joinToString("\n")
        }
    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}

