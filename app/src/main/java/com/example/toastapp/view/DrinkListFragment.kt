package com.example.toastapp.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.toastapp.adapter.CategoryDrinksAdapter
import com.example.toastapp.databinding.FragmentDrinkListBinding
import com.example.toastapp.local.dao.entity.Drinks
import com.example.toastapp.viewModel.CategoryDrinksViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DrinkListFragment : Fragment() {

    private var _binding: FragmentDrinkListBinding? = null
    private val binding get() = _binding!!
    private val categoryDrinksViewModel by viewModels<CategoryDrinksViewModel>()
    private val categoryDrinksAdapter by lazy {
        CategoryDrinksAdapter(::drinkClicked,
            ::favClicked)
    }
    private val args by navArgs<DrinkListFragmentArgs>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ) = FragmentDrinkListBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initObserver()
    }


    private fun initObserver() {
        categoryDrinksViewModel.drinkState.observe(viewLifecycleOwner) { state ->
            categoryDrinksAdapter.displayDrinks(state.drinksList)
            binding.rvDrinks.adapter = categoryDrinksAdapter
        }
        val category = args.category
        categoryDrinksViewModel.getCategoryDrinks(category)
    }

    private fun drinkClicked(drink: Drinks) {
        val drinkNav = DrinkListFragmentDirections.actionDrinkListFragmentToDrinkDetailFragment(
            drinkId = drink.idDrink)
        findNavController().navigate(drinkNav)
    }

    private fun favClicked(drink: Drinks) {
        val copy = drink.copy(isFavorited = true)
        categoryDrinksViewModel.getFavoriteDrinks(copy.isFavorited)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}