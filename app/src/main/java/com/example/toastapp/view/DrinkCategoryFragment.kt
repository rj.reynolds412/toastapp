package com.example.toastapp.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.example.toastapp.adapter.CategoryAdapter
import com.example.toastapp.databinding.FragmentDrinkCategoryBinding
import com.example.toastapp.domain.GrabCategoryUseCase
import com.example.toastapp.factory.DrinkCategoryViewModelFactory
import com.example.toastapp.viewModel.DrinkCategoryViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class DrinkCategoryFragment : Fragment() {

    private var _binding: FragmentDrinkCategoryBinding? = null
    private val binding get() = _binding!!
    @Inject
    lateinit var useCase: GrabCategoryUseCase

    private val drinkCategoryViewModel: DrinkCategoryViewModel by viewModels { DrinkCategoryViewModelFactory(useCase) }
    private val categoryAdapter by lazy { CategoryAdapter() }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ) = FragmentDrinkCategoryBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initObservers()
    }

    private fun initObservers() = with(binding) {
        rvCategory.adapter = categoryAdapter
        drinkCategoryViewModel.categoryState.observe(viewLifecycleOwner) { state ->
            categoryAdapter.displayCategory(state.categoryResponse)
        }
        drinkCategoryViewModel.getCategory(category = "list")
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}