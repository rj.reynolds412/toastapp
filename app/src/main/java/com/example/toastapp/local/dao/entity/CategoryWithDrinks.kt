package com.example.toastapp.local.dao.entity

import androidx.room.Embedded
import androidx.room.Relation

data class CategoryWithDrinks(
    @Embedded val category: Category,
    @Relation(
        parentColumn = "strCategory",
        entityColumn = "strCategory"
    )
    val drinks : List<Drinks>
)
