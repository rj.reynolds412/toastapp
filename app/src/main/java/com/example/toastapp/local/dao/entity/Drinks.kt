package com.example.toastapp.local.dao.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "drinks")
data class Drinks(
    @PrimaryKey(autoGenerate = false)
    val idDrink: Int = 0,
    val strDrink: String? = "",
    val strDrinkThumb: String? = "",
    val strInstructions: String? = "",
    val strCategory: String? = "",
    val strIngredient1: String? = "",
    val strIngredient2: String? = "",
    val strIngredient3: String? = "",
    val strIngredient4: String? = "",
    val strMeasure1: String? = "",
    val strMeasure2: String? = "",
    val strMeasure3: String? = "",
    var isFavorited: Boolean = false,
)
