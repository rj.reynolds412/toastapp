package com.example.toastapp.local.dao.entity

import androidx.annotation.NonNull
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "categories")
data class Category(
    @PrimaryKey(autoGenerate = false)
    val strCategory: String,
    val strDrinkThumb: String = "",
)