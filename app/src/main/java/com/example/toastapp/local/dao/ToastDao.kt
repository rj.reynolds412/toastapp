package com.example.toastapp.local.dao

import androidx.room.*
import com.example.toastapp.local.dao.entity.Category
import com.example.toastapp.local.dao.entity.CategoryWithDrinks
import com.example.toastapp.local.dao.entity.Drinks

@Dao
interface ToastDao {
    @Query("SELECT * FROM drinks")
    suspend fun getAllDrinks(): List<Drinks>

    @Query("SELECT * FROM categories")
    suspend fun getAllCategories(): List<Category>

    @Insert(entity = Category::class)
    suspend fun insertCategories(category: List<Category>)

    @Insert(entity = Drinks::class)
    suspend fun insertDrinks(drinks: List<Drinks>)

    @Transaction
    @Query("SELECT * FROM categories WHERE strCategory is :category ORDER BY strCategory ASC")
    fun getCategoryWithDrinks(category: String): CategoryWithDrinks

    @Query("SELECT * FROM drinks WHERE idDrink is :id ORDER BY strDrink ASC")
    fun getDrinkById(id : Int) : Drinks

    @Query("SELECT * FROM drinks WHERE isFavorited is :favorite" )
    fun getFavoriteDrink(favorite: Boolean) : Drinks

    @Update
    fun updateDrink(drink: Drinks)
}