package com.example.toastapp.local.dao

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.toastapp.local.dao.entity.Category
import com.example.toastapp.local.dao.entity.Drinks

@Database(entities = [Drinks::class, Category::class],
    exportSchema = false,
    version = 1)
abstract class ToastDatabase : RoomDatabase() {

    abstract fun toastDao(): ToastDao
}