package com.example.toastapp.factory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.toastapp.domain.GrabCategoryUseCase
import com.example.toastapp.viewModel.DrinkCategoryViewModel


class DrinkCategoryViewModelFactory(val useCase: GrabCategoryUseCase) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(DrinkCategoryViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return DrinkCategoryViewModel(useCase) as T
        }
        throw IllegalArgumentException("Unable to construct viewmodel")
    }
}
