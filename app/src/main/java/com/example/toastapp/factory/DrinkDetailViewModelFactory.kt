package com.example.toastapp.factory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.toastapp.domain.GrabDrinkDetailUseCase
import com.example.toastapp.viewModel.DrinkDetailViewModel

class DrinkDetailViewModelFactory(val useCase: GrabDrinkDetailUseCase) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(DrinkDetailViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return DrinkDetailViewModel(useCase) as T
        }
        throw IllegalArgumentException("Unable to construct viewmodel")
    }
}
