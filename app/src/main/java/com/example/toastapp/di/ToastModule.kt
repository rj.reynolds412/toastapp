package com.example.toastapp.di

import android.app.Application
import androidx.room.Room
import com.example.toastapp.domain.GrabCategoryUseCase
import com.example.toastapp.factory.DrinkCategoryViewModelFactory
import com.example.toastapp.local.dao.ToastDatabase
import com.example.toastapp.model.ToastService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class ToastModule {

    @Provides
    @Singleton
    fun providesToastService(): ToastService = Retrofit.Builder()
        .baseUrl(ToastService.BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .build()
        .create(ToastService::class.java)

    @Provides
    @Singleton
    fun providesViewModelFactory(useCase: GrabCategoryUseCase): DrinkCategoryViewModelFactory =
        DrinkCategoryViewModelFactory(useCase)

    @Provides
    @Singleton
    fun provideDatabase(app: Application) =
        Room.databaseBuilder(app, ToastDatabase::class.java, "TOAST_DB").build()

    @Provides
    @Singleton
    fun provideToastDao(database: ToastDatabase) = database.toastDao()

}