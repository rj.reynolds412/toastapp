package com.example.toastapp.model

import com.example.toastapp.local.dao.ToastDao
import com.example.toastapp.local.dao.entity.Category
import com.example.toastapp.local.dao.entity.CategoryWithDrinks
import com.example.toastapp.local.dao.entity.Drinks
import com.example.toastapp.model.response.CategoryDrinksResponseDTO
import com.example.toastapp.model.response.CategoryResponseDTO
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ToastRepo @Inject constructor(
    private val toastService: ToastService,
    private val toastDao: ToastDao,
) {


    suspend fun getCategory(strCategory: String): List<Category> = withContext(Dispatchers.IO) {
        // Check DB
        val categoryCache: List<Category> = toastDao.getAllCategories()
        // If DB empty call Cocktails API
        return@withContext categoryCache.ifEmpty {
            val categoryDTO: CategoryResponseDTO = toastService.grabCategory(strCategory)
            // Map DTO to entities
            val dtoList: List<Category> = categoryDTO.listCategory.map {
                Category(strCategory = it.strCategory)
            }
            // Store entities
            toastDao.insertCategories(dtoList)
            return@ifEmpty dtoList
        }
    }

    suspend fun getCategoryDrinks(categoryName: String): List<Drinks> =
        withContext(Dispatchers.IO) {
            // Check DB for drinks in category
            val categoryWithDrinksDb: CategoryWithDrinks =
                toastDao.getCategoryWithDrinks(categoryName)

            return@withContext categoryWithDrinksDb.drinks.ifEmpty {
                val categoryDrinksDTO: CategoryDrinksResponseDTO =
                    toastService.grabCategoryDrinks(categoryName)

                val drinkList: List<Drinks> = categoryDrinksDTO.drinks.map {
                    Drinks(
                        idDrink = it.idDrink.toInt(),
                        strDrink = it.strDrink,
                        strDrinkThumb = it.strDrinkThumb,
                        strInstructions = it.strInstructions,
                        strCategory = categoryName,
                        strIngredient1 = it.strIngredient1,
                        strIngredient2 = it.strIngredient2,
                        strIngredient3 = it.strIngredient3,
                        strIngredient4 = it.strIngredient4,
                        strMeasure1 = it.strMeasure1,
                        strMeasure2 = it.strMeasure2,
                        strMeasure3 = it.strMeasure3,
                    )
                }

                toastDao.insertDrinks(drinkList)
                return@ifEmpty drinkList
            }
        }

    suspend fun getDrinkById(id: Int) = withContext(Dispatchers.IO) {
        val drinkFromDb = toastDao.getDrinkById(id)
        if (drinkFromDb.strInstructions.isNullOrEmpty() || drinkFromDb.strIngredient1.isNullOrEmpty()) {
            val dto = toastService.grabDrinkDetails(id)
            val drink = dto.drinks.firstOrNull()
            Timber.d("Drink by id %s", drink.toString())
            val drinkEntity = drinkFromDb.copy(
                strInstructions = drink?.strInstructions,
                strIngredient1 = drink?.strIngredient1,
                strIngredient2 = drink?.strIngredient2,
                strIngredient3 = drink?.strIngredient3,
                strIngredient4 = drink?.strIngredient4,
            )
            toastDao.updateDrink(drinkEntity)
            return@withContext drinkEntity
        } else {
            drinkFromDb
        }
    }

    suspend fun getFavoriteDrinks(favorite: Boolean) = withContext(Dispatchers.IO) {
        val savedFavs = toastDao.getFavoriteDrink(favorite)
        if (savedFavs.isFavorited != favorite){
            toastDao.getFavoriteDrink(favorite)
            val favDrink = savedFavs.copy(
                isFavorited = false
            )
            toastDao.updateDrink(savedFavs)
            return@withContext favDrink
        }
        else{
            savedFavs
        }
    }
}