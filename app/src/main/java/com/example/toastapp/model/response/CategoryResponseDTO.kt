package com.example.toastapp.model.response

import com.example.toastapp.local.dao.entity.Category
import com.google.gson.annotations.SerializedName


data class CategoryResponseDTO(
    @SerializedName("drinks")
    val listCategory: List<CategoryDTO>,
){
    data class CategoryDTO(
        val strCategory:String = ""
    )
}


