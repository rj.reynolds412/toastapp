package com.example.toastapp.model.response

import com.example.toastapp.local.dao.entity.Drinks


data class CategoryDrinksResponseDTO(
    val drinks: List<CategoryDrinksDTO>,
) {
    data class CategoryDrinksDTO(
        val idDrink: String,
        val strDrink: String? = "",
        val strDrinkThumb: String? = "",
        val strInstructions: String? = "",
        val strIngredient1: String? = "",
        val strIngredient2: String? = "",
        val strIngredient3: String? = "",
        val strIngredient4: String? = "",
        val strMeasure1: String? = "",
        val strMeasure2: String? = "",
        val strMeasure3: String? = "",
    )
}

//fun CategoryDrinksResponseDTO.CategoryDrinksDTO.mapToEntity() = Drinks(
//    idDrink.toInt(),
//    strDrink,
//    s
//)