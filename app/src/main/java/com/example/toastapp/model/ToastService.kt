package com.example.toastapp.model

import com.example.toastapp.model.response.CategoryResponseDTO
import com.example.toastapp.model.response.CategoryDrinksResponseDTO
import retrofit2.http.GET
import retrofit2.http.Query

interface ToastService {
    companion object {
        const val BASE_URL = "https://www.thecocktaildb.com"
    }

    @GET("/api/json/v1/1/list.php")
    suspend fun grabCategory(@Query("c") drinkType: String): CategoryResponseDTO

    @GET("/api/json/v1/1/filter.php")
    suspend fun grabCategoryDrinks(@Query("c") drinkCategory: String): CategoryDrinksResponseDTO

    @GET("/api/json/v1/1/lookup.php")
    suspend fun grabDrinkDetails(@Query("i") id: Int): CategoryDrinksResponseDTO


}